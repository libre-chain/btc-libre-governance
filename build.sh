mkdir -p build
cd build
cmake `clsdk-cmake-args` ..
make -j $(nproc)
cltester -v test-dao.wasm
cd ..

pwd
cp build/btclgovernan.* ./btclgovernan/  || true
