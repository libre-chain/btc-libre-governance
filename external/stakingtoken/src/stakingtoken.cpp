#include <eosio/system.hpp>
#include <math.h>
#include <stakingtoken.hpp>
#include <token/token.hpp>

namespace libre {
  void stakingtoken::notify_transfer( name   from,
                                      name   to,
                                      asset  quantity,
                                      string memo ) {
    // skip transactions that are not related
    if ( from == get_self() || to != get_self() ) {
      return;
    }

    vector< string > params = get_params( memo );

    eosio::check( params.size() == 2,
                  "Invalid memo: expected format is 'action:arg'" );

    uint64_t length = convert_str_to_uint64( params[1] );

    if ( params[0] == "stakefor" ) {
      eosio::check( SUPPORTED_TOKEN_CONTRACT == get_first_receiver(),
                    "Invalid contract" );
      eosio::check( SUPPORTED_TOKEN_SYMBOL == quantity.symbol,
                    "Invalid token symbol" );

      save_stake( from, length, quantity );

      return;
    }

    if ( params[0] == "contributefor" ) {
      eosio::check( SUPPORTED_MINT_TOKEN_CONTRACT == get_first_receiver(),
                    "Invalid contract" );
      eosio::check( SUPPORTED_MINT_TOKEN_SYMBOL == quantity.symbol,
                    "Invalid token symbol" );

      save_mint( from, length, quantity );

      return;
    }

    check( false, "invalid memo" );
  }

  void stakingtoken::init( time_point_sec start_date, uint16_t total_days ) {
    require_auth( get_self() );

    token::stats stats_tb( SUPPORTED_TOKEN_CONTRACT,
                           SUPPORTED_TOKEN_SYMBOL.code().raw() );
    auto         acc_itr = stats_tb.find( SUPPORTED_TOKEN_SYMBOL.code().raw() );

    check( acc_itr != stats_tb.end(),
           "Supported token symbol is not initialized" );
    check( acc_itr->max_supply >= TOTAL_LIBRE_TOKEN,
           "TOTAL_LIBRE_TOKEN exceeds the max supply for: " +
               SUPPORTED_TOKEN_SYMBOL.code().to_string() + " token" );
    check( start_date.sec_since_epoch() >=
               eosio::current_time_point().sec_since_epoch(),
           "Start date is passed" );
    check( total_days >= MIN_MINT_LENGTH_DAYS,
           "End date must be at least " +
               std::to_string( MIN_MINT_LENGTH_DAYS ) + " day apart" );
    check( !stats_sing.exists(), "Singleton is already initialized" );

    stats_sing.set(
        mintstats{ .btc_contributed = asset( 0, SUPPORTED_MINT_TOKEN_SYMBOL ),
                   .start_date = start_date,
                   .end_date = start_date + eosio::days( total_days ),
                   .total_days = total_days },
        get_self() );
    state_sing.set( state{ .next_update = eosio::current_time_point() },
                    get_self() );
  }

  void stakingtoken::claim( name account, uint64_t index ) {
    require_auth( account );

    auto row = stake_tb.find( index );

    check( row != stake_tb.end(), "no records found" );
    check( row->account == account,
           "stake not belong to the provided account" );
    check( row->status == stake_status::STAKE_IN_PROGRESS,
           "invalid status, must be STAKE_IN_PROGRESS" );
    check( row->payout_date.sec_since_epoch() <
               eosio::current_time_point().sec_since_epoch(),
           "invalid date, not yet ready to claim" );

    eosio::asset new_tokens = row->payout - row->libre_staked;
    eosio::asset additional_tokens = INFLATION_PCT * new_tokens / 100;
    eosio::name  temp_referrer = get_referrer( row->account );
    eosio::name  referrer = temp_referrer != eosio::name( -1 )
                                ? temp_referrer
                                : DEFAULT_REFERRER_ACCOUNT;

    if ( new_tokens.amount > 0 ) {
      // issue new_tokens
      action( permission_level{ TOKEN_ISSUER, eosio::name( "stake" ) },
              eosio::name( SUPPORTED_TOKEN_CONTRACT ),
              eosio::name( "issue" ),
              std::make_tuple( TOKEN_ISSUER,
                               new_tokens + ( additional_tokens * 2 ),
                               "claim for stake #" + to_string( index ) ) )
          .send();

      // send new_tokens
      action( permission_level{ TOKEN_ISSUER, eosio::name( "stake" ) },
              eosio::name( SUPPORTED_TOKEN_CONTRACT ),
              eosio::name( "transfer" ),
              std::make_tuple( TOKEN_ISSUER,
                               row->account,
                               new_tokens,
                               "claim for stake #" + to_string( index ) ) )
          .send();
    }

    // send libre_staked
    action( permission_level{ get_self(), eosio::name( "active" ) },
            eosio::name( SUPPORTED_TOKEN_CONTRACT ),
            eosio::name( "transfer" ),
            std::make_tuple( get_self(),
                             row->account,
                             row->libre_staked,
                             "claim for stake #" + to_string( index ) ) )
        .send();

    if ( additional_tokens.amount > 0 ) {
      // send tokens to dao
      action( permission_level{ TOKEN_ISSUER, eosio::name( "stake" ) },
              eosio::name( SUPPORTED_TOKEN_CONTRACT ),
              eosio::name( "transfer" ),
              std::make_tuple( TOKEN_ISSUER,
                               DAO_ACCOUNT,
                               additional_tokens,
                               DONATION_TRANSFER + ": DAO contribution from " +
                                   account.to_string() ) )
          .send();

      // send tokens to referrer
      action( permission_level{ TOKEN_ISSUER, eosio::name( "stake" ) },
              eosio::name( SUPPORTED_TOKEN_CONTRACT ),
              eosio::name( "transfer" ),
              std::make_tuple( TOKEN_ISSUER,
                               referrer,
                               additional_tokens,
                               "Bonus from referral's claim: " +
                                   account.to_string() ) )
          .send();
    }

    stake_tb.modify( row, get_self(), [&]( auto &ref ) {
      ref.status = stake_status::STAKE_COMPLETED;
    } );

    action( permission_level{ get_self(), eosio::name( "active" ) },
            EOSIO_CONTRACT,
            EOSIO_CONTRACT_ACTION,
            std::make_tuple( account ) )
        .send();
  }

  void stakingtoken::unstake( name account, uint64_t index ) {
    require_auth( account );

    auto row = stake_tb.find( index );

    check( row != stake_tb.end(), "no records found" );
    check( row->account == account, "account not match" );
    check( row->status == stake_status::STAKE_IN_PROGRESS,
           "status must be STAKE_IN_PROGRESS" );

    // send libre_staked less penalty
    asset penalty = asset( row->libre_staked.amount * UNSTAKE_PENALTY,
                           SUPPORTED_TOKEN_SYMBOL );
    action( permission_level{ get_self(), eosio::name( "active" ) },
            eosio::name( SUPPORTED_TOKEN_CONTRACT ),
            eosio::name( "transfer" ),
            std::make_tuple( get_self(),
                             row->account,
                             row->libre_staked - penalty,
                             "unstake penalty " + penalty.to_string() ) )
        .send();

    // send penalty to DAO account
    action( permission_level{ get_self(), eosio::name( "active" ) },
            eosio::name( SUPPORTED_TOKEN_CONTRACT ),
            eosio::name( "transfer" ),
            std::make_tuple( get_self(),
                             DAO_ACCOUNT,
                             penalty,
                             "unstake penalty from " + account.to_string() ) )
        .send();

    stake_tb.modify( row, get_self(), [&]( auto &ref ) {
      ref.status = stake_status::STAKE_CANCELED;
    } );

    // update the vote weight in the system contract
    action( permission_level{ get_self(), eosio::name( "active" ) },
            eosio::name( "eosio" ),
            eosio::name( "vonstake" ),
            std::make_tuple( account ) )
        .send();
  }

  void stakingtoken::updatevp( uint32_t max_steps ) {
    check( handle_voting_power( max_steps ) != max_steps, "Nothing to do" );
  }

  void stakingtoken::mintprocess( uint32_t max_steps ) {
    eosio::check( stats_sing.exists(),
                  "The smart contract must be initialized first" );

    uint32_t copy_max_steps = max_steps;
    auto     stats_info = stats_sing.get();
    auto     status_index = mint_tb.get_index< name( "status" ) >();

    eosio::check( eosio::current_time_point().sec_since_epoch() >=
                      stats_info.end_date.sec_since_epoch(),
                  "The mint rush has not finished yet" );

    for ( auto itr = status_index.lower_bound( mint_status::MINT_IN_PROGRESS );
          itr != status_index.upper_bound( mint_status::MINT_IN_PROGRESS ) &&
          max_steps > 0;
          itr = status_index.lower_bound( mint_status::MINT_IN_PROGRESS ),
               --max_steps ) {
      const double libre = (double)TOTAL_LIBRE_TOKEN.amount *
                           (double)itr->btc_contributed.amount /
                           (double)stats_info.btc_contributed.amount;
      const asset libre_minted = asset( libre, SUPPORTED_TOKEN_SYMBOL );

      save_mint2stake( itr->account,
                       itr->staked_days,
                       libre_minted,
                       itr->mint_bonus / 100,
                       itr->mint_apy / 100 );

      status_index.modify( itr, get_self(), [&]( auto &ref ) {
        ref.libre_minted = libre_minted;
        ref.status = mint_status::MINT_COMPLETED;
      } );
    }

    eosio::check( copy_max_steps != max_steps, "Nothing to do" );
  }

  void stakingtoken::stakepreview( name           account,
                                   time_point_sec stake_date,
                                   uint64_t       stake_length,
                                   float          mint_bonus,
                                   asset          libre_staked ) {
    require_auth( account );

    check( libre_staked.symbol == SUPPORTED_TOKEN_SYMBOL,
           "Invalid token symbol" );
    check( stake_length >= MIN_STAKE_LENGTH_DAYS,
           "You must stake for at least " +
               std::to_string( MIN_STAKE_LENGTH_DAYS ) + " day" );
    check( stake_length <= MAX_STAKE_LENGTH_DAYS,
           "You can stake up to " + std::to_string( MAX_STAKE_LENGTH_DAYS ) +
               " days" );
    check( S_L <= stake_date, "Staking has not started yet" );

    const double apy =
        calculate_apy( stake_date, stake_length, mint_bonus / 100 );
    const asset payout = calculate_payout( apy, stake_length, libre_staked );
    const time_point_sec payout_date = stake_date + eosio::days( stake_length );

    SEND_INLINE_ACTION( *this,
                        stakelog,
                        { { get_self(), name( "active" ) } },
                        { UINT64_MAX,
                          account,
                          stake_date,
                          stake_length,
                          mint_bonus,
                          libre_staked,
                          apy * 100,
                          payout,
                          payout_date,
                          stake_status::STAKE_PREVIEW } );
  }

  void stakingtoken::mintpreview( name           account,
                                  time_point_sec mint_date,
                                  uint64_t       mint_length,
                                  asset          btc_contributed ) {
    require_auth( account );

    check( btc_contributed.symbol == SUPPORTED_MINT_TOKEN_SYMBOL,
           "Invalid token symbol" );
    check( mint_length >= stats_sing.get().total_days,
           "You must stake for at least " +
               std::to_string( stats_sing.get().total_days ) + " days" );
    check( mint_length <= MAX_MINT_LENGTH_DAYS,
           "You can stake up to " + std::to_string( MAX_MINT_LENGTH_DAYS ) +
               " days" );
    check( mint_date >= stats_sing.get().start_date,
           "Mintrush has not started yet" );

    uint64_t day_of_mint_rush =
        get_days_diff( mint_date, stats_sing.get().start_date );
    double mint_bonus = calculate_mint_bonus( day_of_mint_rush );
    double mint_apy = calculate_mint_apy( mint_length, mint_bonus );

    SEND_INLINE_ACTION( *this,
                        mintlog,
                        { { get_self(), name( "active" ) } },
                        { UINT64_MAX,
                          account,
                          day_of_mint_rush,
                          mint_length,
                          btc_contributed,
                          mint_bonus * 100,
                          asset(),
                          mint_apy * 100,
                          mint_status::MINT_PREVIEW } );
  }

  void stakingtoken::stakelog( uint64_t       index,
                               name           account,
                               time_point_sec stake_date,
                               uint64_t       stake_length,
                               float          mint_bonus,
                               asset          libre_staked,
                               float          apy,
                               asset          payout,
                               time_point_sec payout_date,
                               uint64_t       status ) {
    require_auth( get_self() );
  }

  void stakingtoken::mintlog( uint64_t index,
                              name     account,
                              uint64_t day_of_mint_rush,
                              uint64_t staked_days,
                              asset    btc_contributed,
                              float    mint_bonus,
                              asset    libre_minted,
                              float    mint_apy,
                              uint64_t status ) {
    require_auth( get_self() );
  }

  void stakingtoken::migrate1( uint32_t max_steps ) {
    auto mig = mig_sing.get_or_create( get_self(), migrate{} );

    eosio::check( mig.last_index != UINT64_MAX, "Migration #1 is completed" );

    auto itr = mig.last_index != 0 ? stake_tb.find( mig.last_index )
                                   : stake_tb.begin();
    auto end = stake_tb.end();

    for ( ; itr != end && max_steps > 0; --max_steps, ++itr ) {
      auto temp_itr = power_tb.find( itr->account.value );

      if ( temp_itr == power_tb.end() ) {
        power_tb.emplace( get_self(),
                          [&]( auto &row ) { row.account = itr->account; } );
      }
    }

    mig.last_index = itr != stake_tb.end() ? itr->index : UINT64_MAX;
    mig_sing.set( mig, get_self() );
  }

  void stakingtoken::migrate2( uint32_t max_steps ) {
    // this table is not going to be required any more because of STK-07
    balance_table balance_tb( get_self(), get_self().value );
    auto          itr = balance_tb.begin();
    auto          end_itr = balance_tb.end();

    for ( ; itr != end_itr && max_steps > 0; --max_steps ) {
      itr = balance_tb.erase( itr );
    }

    if ( max_steps == 0 ) {
      return;
    }

    global_table glo_sing( get_self(), get_self().value );

    if ( !glo_sing.exists() ) {
      eosio::check( false, "Migration #2 is completed" );
    }

    glo_sing.remove();

    state_sing.get_or_create(
        get_self(),
        state{ .next_update = eosio::current_time_point() } );
  }

  vector< string > stakingtoken::get_params( string memo ) {
    vector< string > params;
    string           param;
    size_t           pos = 0;
    string           delimiter = ":";

    while ( ( pos = memo.find( delimiter ) ) != string::npos ) {
      param = memo.substr( 0, pos );
      params.push_back( param );
      memo.erase( 0, pos + delimiter.length() );
    }

    if ( memo.length() > 0 ) {
      params.push_back( memo );
    }

    return params;
  }

  uint64_t stakingtoken::get_days_diff( time_point_sec date1,
                                        time_point_sec date2 ) {
    return ( date1.utc_seconds - date2.utc_seconds ) / 60 / 60 / 24;
  }

  uint64_t stakingtoken::convert_str_to_uint64( const string &str ) {
    uint64_t num = 0;

    for ( char c : str ) {
      if ( !isdigit( c ) ) {
        eosio::check( false, "Invalid number: " + str );
      }

      uint64_t digit = c - '0';

      if ( num > ( UINT64_MAX - digit ) / 10 ) {
        eosio::check( false, "Overflow: " + str );
      }

      num = num * 10 + digit;
    }

    return num;
  }

  bool stakingtoken::check_update_voting_power() {
    return eosio::current_time_point().sec_since_epoch() >=
           state_sing.get().next_update.sec_since_epoch();
  }

  void stakingtoken::save_stake( name     account,
                                 uint64_t stake_length,
                                 asset    libre_staked ) {
    check( stake_length >= MIN_STAKE_LENGTH_DAYS,
           "You must stake for at least " +
               std::to_string( MIN_STAKE_LENGTH_DAYS ) + " day" );
    check( stake_length <= MAX_STAKE_LENGTH_DAYS,
           "You can stake up to " + std::to_string( MAX_STAKE_LENGTH_DAYS ) +
               " days" );

    const time_point_sec stake_date = eosio::current_time_point();

    check( S_L <= stake_date, "Staking has not started yet" );

    float        mint_bonus = 0;
    const double apy =
        calculate_apy( stake_date, stake_length, mint_bonus / 100 );
    const asset payout = calculate_payout( apy, stake_length, libre_staked );
    const time_point_sec payout_date = stake_date + eosio::days( stake_length );

    auto new_row = stake_tb.emplace( get_self(), [&]( auto &row ) {
      row.index = stake_tb.available_primary_key();
      row.account = account;
      row.stake_date = stake_date;
      row.stake_length = stake_length;
      row.mint_bonus = mint_bonus;
      row.libre_staked = libre_staked;
      row.apy = apy * 100;
      row.payout = payout;
      row.payout_date = payout_date;
      row.status = stake_status::STAKE_IN_PROGRESS;
    } );

    register_voting_power( account, libre_staked, payout_date );

    // log the stake info
    SEND_INLINE_ACTION( *this,
                        stakelog,
                        { { get_self(), name( "active" ) } },
                        { new_row->index,
                          account,
                          stake_date,
                          stake_length,
                          mint_bonus,
                          libre_staked,
                          apy * 100,
                          payout,
                          payout_date,
                          stake_status::STAKE_IN_PROGRESS } );

    // update the vote weight in the system contract
    action( permission_level{ get_self(), eosio::name( "active" ) },
            eosio::name( "eosio" ),
            eosio::name( "vonstake" ),
            std::make_tuple( account ) )
        .send();
  }

  void stakingtoken::save_mint( name     account,
                                uint64_t mint_length,
                                asset    btc_contributed ) {
    check( mint_length >= stats_sing.get().total_days,
           "You must stake for at least " +
               std::to_string( stats_sing.get().total_days ) + " days" );
    check( mint_length <= MAX_MINT_LENGTH_DAYS,
           "You can stake up to " + std::to_string( MAX_MINT_LENGTH_DAYS ) +
               " days" );

    const time_point_sec mint_date = eosio::current_time_point();

    check( mint_date >= stats_sing.get().start_date,
           "Mintrush has not started yet" );

    uint64_t day_of_mint_rush =
        get_days_diff( mint_date, stats_sing.get().start_date );

    check( day_of_mint_rush <= stats_sing.get().total_days,
           "Mint is only available for the " +
               std::to_string( stats_sing.get().total_days ) +
               " days from the start of the Mint Rush" );

    const double mint_bonus = calculate_mint_bonus( day_of_mint_rush );
    const double mint_apy = calculate_mint_apy( mint_length, mint_bonus );

    auto new_row = mint_tb.emplace( get_self(), [&]( auto &row ) {
      row.index = mint_tb.available_primary_key();
      row.account = account;
      row.day_of_mint_rush = day_of_mint_rush;
      row.staked_days = mint_length;
      row.btc_contributed = btc_contributed;
      row.mint_bonus = mint_bonus * 100;
      row.libre_minted = asset();
      row.mint_apy = mint_apy * 100;
      row.status = mint_status::MINT_IN_PROGRESS;
    } );

    auto stats_info = stats_sing.get();
    stats_info.btc_contributed += btc_contributed;
    stats_sing.set( stats_info, get_self() );

    // log the stake info
    SEND_INLINE_ACTION( *this,
                        mintlog,
                        { { get_self(), name( "active" ) } },
                        { new_row->index,
                          account,
                          day_of_mint_rush,
                          mint_length,
                          btc_contributed,
                          mint_bonus * 100,
                          asset(),
                          mint_apy * 100,
                          mint_status::MINT_IN_PROGRESS } );
  }

  void stakingtoken::save_mint2stake( name     account,
                                      uint64_t stake_length,
                                      asset    libre_staked,
                                      double   pct_mint_bonus,
                                      double   pct_apy ) {
    const time_point_sec stake_date = eosio::current_time_point();
    // Staking does not compound for multi-year stakes
    const asset payout =
        asset( (double)libre_staked.amount * pct_apy *
                       ( (double)stake_length / ONE_YEAR_DAYS ) +
                   (double)libre_staked.amount,
               libre_staked.symbol );
    const time_point_sec payout_date = stake_date + eosio::days( stake_length );

    auto new_row = stake_tb.emplace( get_self(), [&]( auto &row ) {
      row.index = stake_tb.available_primary_key();
      row.account = account;
      row.stake_date = stake_date;
      row.stake_length = stake_length;
      row.mint_bonus = pct_mint_bonus;
      row.libre_staked = libre_staked;
      row.apy = pct_apy * 100;
      row.payout = payout;
      row.payout_date = payout_date;
      row.status = stake_status::STAKE_IN_PROGRESS;
    } );

    register_voting_power( account, libre_staked, payout_date );

    // log the stake info
    SEND_INLINE_ACTION( *this,
                        stakelog,
                        { { get_self(), name( "active" ) } },
                        { new_row->index,
                          account,
                          stake_date,
                          stake_length,
                          pct_mint_bonus,
                          libre_staked,
                          pct_apy * 100,
                          payout,
                          payout_date,
                          stake_status::STAKE_IN_PROGRESS } );

    // update the vote weight in the system contract
    action( permission_level{ get_self(), eosio::name( "active" ) },
            eosio::name( "eosio" ),
            eosio::name( "vonstake" ),
            std::make_tuple( account ) )
        .send();
  }

  void stakingtoken::register_voting_power( name           account,
                                            asset          libre_staked,
                                            time_point_sec payout_date ) {
    auto itr = power_tb.find( account.value );

    if ( itr == power_tb.end() ) {
      itr = power_tb.emplace( get_self(),
                              [&]( auto &row ) { row.account = account; } );
    }

    auto     ctp = eosio::current_time_point();
    auto     ctp_sse = ctp.sec_since_epoch();
    uint64_t days_remaining = get_days_diff( payout_date, ctp );
    double   voting_power =
        calc_staking_power( days_remaining, libre_staked.amount );

    power_tb.modify( itr, eosio::same_payer, [&]( auto &row ) {
      row.voting_power += voting_power;
    } );
  }

  uint32_t stakingtoken::handle_voting_power( uint32_t max_steps ) {
    if ( check_update_voting_power() ) {
      auto state = state_sing.get();

      if ( state.vp_state == update_state::STANDBY ) {
        state.vp_state = update_state::CALCULATING_VOTING_POWER;
        state_sing.set( state, get_self() );
        --max_steps;
      }

      if ( state_sing.get().vp_state ==
           update_state::CALCULATING_VOTING_POWER ) {
        update_voting_power( max_steps );
      }

      if ( state_sing.get().vp_state == update_state::REMOVING_VOTING_POWER ) {
        remove_voting_power( max_steps );
      }
    }

    return max_steps;
  }

  void stakingtoken::update_voting_power( uint32_t &max_steps ) {
    auto state = state_sing.get();
    auto ctp = eosio::current_time_point();
    auto ctp_sse = ctp.sec_since_epoch();
    auto itr = state.last_index > 0 ? stake_tb.find( state.last_index )
                                    : stake_tb.begin();
    auto end_itr = stake_tb.end();

    for ( ; max_steps > 0 && itr != end_itr; --max_steps, ++itr ) {
      if ( itr->payout_date.sec_since_epoch() >= ctp_sse &&
           itr->status == stake_status::STAKE_IN_PROGRESS ) {
        uint64_t days_remaining = get_days_diff( itr->payout_date, ctp );
        double   voting_power =
            calc_staking_power( days_remaining, itr->libre_staked.amount );
        auto pwr_itr = power_tb.find( itr->account.value );
        bool is_starting =
            state.next_update - pwr_itr->last_update > eosio::days( 0 );

        power_tb.modify( pwr_itr, eosio::same_payer, [&]( auto &row ) {
          if ( !is_starting ) {
            row.voting_power += voting_power;
          } else {
            row.voting_power = voting_power;
            row.last_update = state.next_update;
          }
        } );
      }
    }

    if ( itr != end_itr ) {
      state.last_index = itr->index;
      state_sing.set( state, get_self() );
    } else {
      state.last_index = 0;
      state.vp_state = update_state::REMOVING_VOTING_POWER;
      state_sing.set( state, get_self() );
    }
  }

  void stakingtoken::remove_voting_power( uint32_t &max_steps ) {
    auto state = state_sing.get();
    auto itr = state.last_index != 0 ? power_tb.find( state.last_index )
                                     : power_tb.begin();
    auto end_itr = power_tb.end();

    for ( ; max_steps > 0 && itr != end_itr; --max_steps, ++itr ) {
      if ( state.next_update - itr->last_update > eosio::days( 0 ) ) {
        power_tb.modify( itr, eosio::same_payer, [&]( auto &row ) {
          row.voting_power = 0;
        } );
      }
    }

    if ( itr != end_itr ) {
      state.last_index = itr->account.value;
      state_sing.set( state, get_self() );
    } else {
      state.last_index = 0;
      state.next_update = eosio::current_time_point() + eosio::minutes( 3 );
      state.vp_state = update_state::STANDBY;
      state_sing.set( state, get_self() );
    }
  }

  double stakingtoken::calculate_apy( time_point_sec stake_date,
                                      uint64_t       stake_length,
                                      float          mint_bonus ) {
    return ( ALPHA0 +
             sqrt( (double)stake_length / ONE_YEAR_DAYS ) * ( BETA0 - ALPHA0 ) +
             ( ALPHAT +
               sqrt( (double)stake_length / ONE_YEAR_DAYS ) *
                   ( BETAT - ALPHAT ) -
               ( ALPHA0 + sqrt( (double)stake_length / ONE_YEAR_DAYS ) *
                              ( BETA0 - ALPHA0 ) ) ) *
                 sqrt( min( (double)1,
                            (double)get_days_diff( stake_date, S_L ) / T ) ) ) *
           ( 1 + mint_bonus );
  }

  double stakingtoken::calculate_mint_bonus( uint64_t day_of_mint_rush ) {
    return (double)1 -
           ( (double)day_of_mint_rush / (double)stats_sing.get().total_days );
  }

  double stakingtoken::calculate_mint_apy( uint64_t mint_length,
                                           double   mint_bonus ) {
    return ( ALPHA0 +
             min( (double)1, sqrt( (double)mint_length / ONE_YEAR_DAYS ) ) *
                 ( BETA0 - ALPHA0 ) ) *
           ( 1 + mint_bonus );
  }

  asset stakingtoken::calculate_payout( double   apy,
                                        uint64_t stake_length,
                                        asset    libre_staked ) {
    // Staking does not compound for multi-year stakes
    int64_t payout_amount = ( ( (double)stake_length / ONE_YEAR_DAYS ) * apy *
                                  (double)libre_staked.amount +
                              (double)libre_staked.amount );

    return asset( payout_amount, libre_staked.symbol );
  }

  double stakingtoken::calc_staking_power( double   days_remaining,
                                           uint64_t libre_staked ) {
    double voting_bonus = min( (double)1, days_remaining / ONE_YEAR_DAYS );

    return ( static_cast< double >( libre_staked ) /
             SUPPORTED_TOKEN_SYMBOL_PRECISION_MULTIPLIER ) *
           ( 1 + voting_bonus );
  }
} // namespace libre

EOSIO_ACTION_DISPATCHER( libre::actions )

EOSIO_ABIGEN( actions( libre::actions ),
              table( "stake"_n, libre::stake ),
              table( "mintstats"_n, libre::mintstats ),
              table( "mintbonus"_n, libre::mintbonus ),
              table( "power"_n, libre::power ),
              table( "migrate"_n, libre::migrate ),
              table( "state"_n, libre::state ) )
