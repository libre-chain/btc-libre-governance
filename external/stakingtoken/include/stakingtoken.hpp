/*
 *
 * @author  EOSCostaRica.io [ https://eoscostarica.io ]
 *
 * @section DESCRIPTION
 *  Header file for the declaration of all functions
 *
 *  GitHub: https://github.com/edenia/staking-contract
 *
 */
#include <eosio/asset.hpp>
#include <eosio/eosio.hpp>
#include <eosio/singleton.hpp>

#include <constants.hpp>

using namespace std;
using namespace eosio;

namespace libre {

  // REFERRAL CONTRACT
  const name REFERRAL_CONTRACT = name( "accts.libre" );

  struct referral {
    name account;
    name referrer;

    uint64_t primary_key() const { return account.value; }
  };
  EOSIO_REFLECT( referral, account, referrer )
  typedef multi_index< "referral"_n, referral > referral_table;

  inline name get_referrer( name acc ) {
    referral_table referral_tb{ REFERRAL_CONTRACT, REFERRAL_CONTRACT.value };
    auto           referral_itr = referral_tb.find( acc.value );

    return referral_itr != referral_tb.end() ? referral_itr->referrer
                                             : eosio::name( -1 );
  }
  // END - REFERRAL CONTRACT

  enum stake_status : uint64_t {
    STAKE_IN_PROGRESS = 1,
    STAKE_COMPLETED = 2,
    STAKE_CANCELED = 3,
    STAKE_PREVIEW = 4
  };

  enum mint_status : uint64_t {
    MINT_IN_PROGRESS = 1,
    MINT_COMPLETED = 2,
    MINT_PREVIEW = 3
  };

  enum update_state : uint8_t {
    STANDBY,
    CALCULATING_VOTING_POWER,
    REMOVING_VOTING_POWER
  };

  struct stake {
    uint64_t       index;
    name           account;
    time_point_sec stake_date;
    uint64_t       stake_length;
    float          mint_bonus;
    asset          libre_staked;
    float          apy;
    asset          payout;
    time_point_sec payout_date;
    uint64_t       status;

    auto     primary_key() const { return index; }
    uint64_t by_account() const { return account.value; }
  };
  EOSIO_REFLECT( stake,
                 index,
                 account,
                 stake_date,
                 stake_length,
                 mint_bonus,
                 libre_staked,
                 apy,
                 payout,
                 payout_date,
                 status )
  typedef multi_index<
      name( "stake" ),
      stake,
      indexed_by< name( "account" ),
                  const_mem_fun< stake, uint64_t, &stake::by_account > > >
      stake_table;

  struct mintstats {
    asset          btc_contributed;
    time_point_sec start_date;
    time_point_sec end_date;
    uint16_t       total_days;
  };
  EOSIO_REFLECT( mintstats, btc_contributed, start_date, end_date, total_days )
  typedef singleton< name( "mintstats" ), mintstats > mintstats_singleton;

  struct mintbonus {
    uint64_t index;
    name     account;
    uint64_t day_of_mint_rush;
    uint64_t staked_days;
    asset    btc_contributed;
    double   mint_bonus;
    asset    libre_minted;
    double   mint_apy;
    uint64_t status;

    auto primary_key() const { return index; }

    uint64_t by_status() const { return status; }
  };
  EOSIO_REFLECT( mintbonus,
                 index,
                 account,
                 day_of_mint_rush,
                 staked_days,
                 btc_contributed,
                 mint_bonus,
                 libre_minted,
                 mint_apy,
                 status )
  typedef multi_index<
      name( "mintbonus" ),
      mintbonus,
      indexed_by<
          name( "status" ),
          const_mem_fun< mintbonus, uint64_t, &mintbonus::by_status > > >
      mint_bonus_table;

  struct balance {
    name           account;
    double         voting_power;
    uint64_t       last_index;
    time_point_sec last_update = time_point_sec();

    auto primary_key() const { return account.value; }
  };
  EOSIO_REFLECT( balance, account, voting_power, last_index, last_update )
  typedef multi_index< name( "balance" ), balance > balance_table;

  struct power {
    name           account;
    double         voting_power;
    time_point_sec last_update = time_point_sec();

    auto primary_key() const { return account.value; }
  };

  EOSIO_REFLECT( power, account, voting_power, last_update )
  typedef multi_index< name( "power" ), power > power_table;

  struct global {
    uint8_t inflation_pct;
    name    last_vp_account_updated;
  };
  EOSIO_REFLECT( global, inflation_pct, last_vp_account_updated )
  typedef eosio::singleton< "global"_n, global > global_table;

  struct migrate {
    uint64_t last_index;
  };
  EOSIO_REFLECT( migrate, last_index )
  typedef eosio::singleton< "migrate"_n, migrate > migrate_singleton;

  struct state {
    eosio::time_point_sec next_update;
    uint64_t              last_index;
    uint8_t               vp_state;
  };
  EOSIO_REFLECT( state, next_update, last_index, vp_state )
  typedef eosio::singleton< "state"_n, state > state_singleton;

  struct stakingtoken : public eosio::contract {
  public:
    using contract::contract;

    stakingtoken( eosio::name                       receiver,
                  eosio::name                       code,
                  eosio::datastream< const char * > ds )
        : contract( receiver, code, ds ), mig_sing( receiver, receiver.value ),
          stats_sing( receiver, receiver.value ),
          state_sing( receiver, receiver.value ),
          mint_tb( receiver, receiver.value ),
          power_tb( receiver, receiver.value ),
          stake_tb( receiver, receiver.value ) {}

    void notify_transfer( name from, name to, asset quantity, string memo );

    void init( time_point_sec start_date, uint16_t total_days );
    void claim( name account, uint64_t index );
    void unstake( name account, uint64_t index );
    void updatevp( uint32_t max_steps );
    void mintprocess( uint32_t max_steps );
    void stakepreview( name           account,
                       time_point_sec stake_date,
                       uint64_t       stake_length,
                       float          mint_bonus,
                       asset          libre_staked );
    void mintpreview( name           account,
                      time_point_sec mint_date,
                      uint64_t       mint_length,
                      asset          btc_contributed );
    void stakelog( uint64_t       index,
                   name           account,
                   time_point_sec stake_date,
                   uint64_t       stake_length,
                   float          mint_bonus,
                   asset          libre_staked,
                   float          apy,
                   asset          payout,
                   time_point_sec payout_date,
                   uint64_t       status );
    void mintlog( uint64_t index,
                  name     account,
                  uint64_t day_of_mint_rush,
                  uint64_t staked_days,
                  asset    btc_contributed,
                  float    mint_bonus,
                  asset    libre_minted,
                  float    mint_apy,
                  uint64_t status );
    void migrate1( uint32_t max_steps );
    void migrate2( uint32_t max_steps );

    vector< string > get_params( string memo );
    uint64_t get_days_diff( time_point_sec date1, time_point_sec date2 );
    uint64_t convert_str_to_uint64( const string &str );
    bool     check_update_voting_power();
    void save_stake( name account, uint64_t stake_length, asset libre_staked );
    void save_mint( name account, uint64_t mint_length, asset btc_contributed );
    void save_mint2stake( name     account,
                          uint64_t stake_length,
                          asset    libre_staked,
                          double   pct_mint_bonus,
                          double   pct_apy );
    void register_voting_power( name           account,
                                asset          libre_staked,
                                time_point_sec payout_date );
    uint32_t handle_voting_power( uint32_t max_steps );
    void     update_voting_power( uint32_t &max_steps );
    void     remove_voting_power( uint32_t &max_steps );
    double   calculate_apy( time_point_sec stake_date,
                            uint64_t       stake_length,
                            float          mint_bonus );
    double   calculate_mint_bonus( uint64_t day_of_mint_rush );
    double   calculate_mint_apy( uint64_t mint_length, double mint_bonus );
    asset
    calculate_payout( double apy, uint64_t stake_length, asset libre_staked );
    double calc_staking_power( double days_remaining, uint64_t libre_staked );

  private:
    migrate_singleton   mig_sing;
    mintstats_singleton stats_sing;
    state_singleton     state_sing;

    mint_bonus_table mint_tb;
    power_table      power_tb;
    stake_table      stake_tb;
  };

  EOSIO_ACTIONS(
      stakingtoken,
      "stake.libre"_n,
      action( init, start_date, total_days ),
      action( claim, account, index ),
      action( unstake, account, index ),
      action( stakepreview,
              account,
              stake_date,
              stake_length,
              mint_bonus,
              libre_staked ),
      action( stakelog,
              index,
              account,
              stake_date,
              stake_length,
              mint_bonus,
              libre_staked,
              apy,
              payout,
              payout_date,
              status ),
      action( mintpreview, account, mint_date, mint_length, btc_contributed ),
      action( mintlog,
              index,
              account,
              day_of_mint_rush,
              staked_days,
              btc_contributed,
              mint_bonus,
              libre_minted,
              mint_apy,
              status ),
      action( mintprocess, max_steps ),
      action( updatevp, max_steps ),
      action( migrate1, max_steps ),
      action( migrate2, max_steps ),
      notify( "eosio.token"_n, transfer ),
      notify( "btc.ptokens"_n, transfer ) )

} // namespace libre