#pragma once

#include <eosio/asset.hpp>
#include <eosio/name.hpp>

namespace dao {
  inline constexpr uint8_t MIN_TIME_VOTES_UPDATE = 5;
  inline constexpr auto    EOSIO_TOKEN_CONTRACT = eosio::name( "eosio.token" );
  inline constexpr auto STAKING_TOKEN_CONTRACT = eosio::name( "stake.libre" );
  inline constexpr auto SUPPORTED_VOTE_TOKEN_SYMBOL =
      eosio::symbol( "LIBRE", 4 );
  inline constexpr auto SUPPORTED_PAY_TOKEN_SYMBOL =
      eosio::symbol( "LIBRE", 4 );
  inline const auto MIN_CREATE_PROPOSAL_AMOUNT =
      eosio::asset( 1, SUPPORTED_VOTE_TOKEN_SYMBOL );
  inline const std::string PAYMENT_TRANSFER = "payment:";
  inline const std::string DONATION_TRANSFER = "donation";
  inline const std::string FOUNDING_TRANSFER = "founding";
} // namespace dao