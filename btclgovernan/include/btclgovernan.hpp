/*
 *
 * @author  EOSCostaRica.io [https://eoscostarica.io]
 *
 * @section DESCRIPTION
 * Header file for the declaration of all functions related with the
 * btclgovernan contract
 *
 * GitHub: https://github.com/eoscostarica/btc-libre-governance
 *
 */
#include <eosio/asset.hpp>
#include <eosio/eosio.hpp>
#include <eosio/singleton.hpp>
#include <eosio/system.hpp>

#include <constants.hpp>

namespace dao {
  // AUXILIARY
  struct stats {
    eosio::asset supply;
    eosio::asset max_supply;
    eosio::name  issuer;

    uint64_t primary_key() const { return supply.symbol.code().raw(); }
  };
  EOSIO_REFLECT( stats, supply, max_supply, issuer )
  using stats_table = eosio::multi_index< eosio::name( "stat" ), stats >;
  // END-AUXILIARY

  enum proposal_status : uint8_t {
    DRAFT = 1,
    ACTIVE = 2,
    SUCCEEDED = 3,
    DEFEATED = 4,
    CANCELED = 5,
    COMPLETED = 6,
  };

  struct params {
    eosio::name  funding_account;
    uint8_t      vote_threshold;
    uint8_t      voting_days;
    eosio::asset minimum_balance_to_create_proposals;
    eosio::asset proposal_cost;
    eosio::name  approver;
  };
  EOSIO_REFLECT( params,
                 funding_account,
                 vote_threshold,
                 voting_days,
                 minimum_balance_to_create_proposals,
                 proposal_cost,
                 approver )
  using params_singleton = eosio::singleton< eosio::name( "params" ), params >;

  struct params2 {
    eosio::name  funding_account;
    uint8_t      vote_threshold;
    uint8_t      voting_days;
    int64_t      minimum_vp_to_create_proposals;
    eosio::asset proposal_cost;
    eosio::name  approver;
  };
  EOSIO_REFLECT( params2,
                 funding_account,
                 vote_threshold,
                 voting_days,
                 minimum_vp_to_create_proposals,
                 proposal_cost,
                 approver )
  using params2_singleton =
      eosio::singleton< eosio::name( "params2" ), params2 >;

  struct proposals {
    eosio::name           name;
    eosio::name           creator;
    eosio::name           receiver;
    std::string           title;
    std::string           detail;
    eosio::asset          amount;
    std::string           url;
    eosio::time_point_sec created_on;
    eosio::time_point_sec expires_on;
    int64_t               votes_for;
    int64_t               votes_against;
    uint8_t               status;

    uint64_t primary_key() const { return name.value; }
    uint64_t by_created_on() const { return created_on.sec_since_epoch(); }
    uint64_t by_expires_on() const { return expires_on.sec_since_epoch(); }
  };
  EOSIO_REFLECT( proposals,
                 name,
                 creator,
                 receiver,
                 title,
                 detail,
                 amount,
                 url,
                 created_on,
                 expires_on,
                 votes_for,
                 votes_against,
                 status )
  using proposal_table = eosio::multi_index<
      eosio::name( "proposals" ),
      proposals,
      eosio::indexed_by< eosio::name( "createdon" ),
                         eosio::const_mem_fun< proposals,
                                               uint64_t,
                                               &proposals::by_created_on > >,
      eosio::indexed_by< eosio::name( "expireson" ),
                         eosio::const_mem_fun< proposals,
                                               uint64_t,
                                               &proposals::by_expires_on > > >;

  struct votes {
    eosio::name voter;
    bool        is_for;
    int64_t     quantity;

    uint64_t primary_key() const { return voter.value; }
  };
  EOSIO_REFLECT( votes, voter, is_for, quantity )
  using vote_table = eosio::multi_index< eosio::name( "votes" ), votes >;

  struct state {
    eosio::name           action;
    eosio::name           next_account;
    eosio::time_point_sec last_update;

    uint64_t primary_key() const { return action.value; }
  };
  EOSIO_REFLECT( state, action, next_account, last_update )
  using state_table = eosio::multi_index< eosio::name( "state" ), state >;

  class btclgovernan : public eosio::contract {
  private:
    params2_singleton param_sing;
    proposal_table    proposal_tb;

  public:
    using contract::contract;

    btclgovernan( eosio::name                       receiver,
                  eosio::name                       code,
                  eosio::datastream< const char * > ds )
        : contract( receiver, code, ds ),
          param_sing( receiver, receiver.value ),
          proposal_tb( receiver, receiver.value ) {}

    void notify_transfer( eosio::name  from,
                          eosio::name  to,
                          eosio::asset quantity,
                          std::string  memo );
    void setparams( eosio::name  funding_account,
                    uint8_t      vote_threshold,
                    uint8_t      voting_days,
                    int64_t      minimum_vp_to_create_proposals,
                    eosio::asset proposal_cost,
                    eosio::name  approver );
    void create( eosio::name  creator,
                 eosio::name  receiver,
                 eosio::name  name,
                 std::string  title,
                 std::string  detail,
                 eosio::asset amount,
                 std::string  url );
    void migrate();
    void votefor( eosio::name voter, eosio::name proposal );
    void voteagainst( eosio::name voter, eosio::name proposal );
    void countvotes( eosio::name proposal, uint32_t max_steps );
    void checkvotes( eosio::name proposal, uint32_t max_steps );
    void approve( eosio::name proposal );
    void reject( eosio::name proposal );

    int64_t get_voting_power( eosio::name account );
    void    payment_handler( eosio::name proposal, eosio::asset quantity );
    void    save_vote( eosio::name voter, eosio::name proposal, bool is_for );
    void    gc( eosio::name action, eosio::name proposal );
  };

  EOSIO_ACTIONS(
      btclgovernan,
      "dao.libre"_n,
      action( setparams,
              funding_account,
              vote_threshold,
              voting_days,
              minimum_vp_to_create_proposals,
              proposal_cost,
              approver ),
      action( create, creator, receiver, name, title, detail, amount, url ),
      action( migrate ),
      action( votefor, voter, proposal ),
      action( voteagainst, voter, proposal ),
      action( countvotes, proposal, max_steps ),
      action( checkvotes, proposal, max_steps ),
      action( approve, proposal ),
      action( reject, proposal ),
      notify( EOSIO_TOKEN_CONTRACT, transfer ) )
} // namespace dao