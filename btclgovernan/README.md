<p align="center">
   <img src="../docs/proton-logo.svg" width="200">
</p>

# BTC Libre Governance Smart Contract

View [project readme](../README.md) document.

The **btclgovernan** smart contract works as a decentralized voting mechanism set up in the rules governing the blockchain, where budgets for specific projects are proposed and funded once they receive sufficient votes from token holders.

# Data Model

Data persisted in the smart contract multi index tables

![Data Model](../docs/data-model.png)

# Actions

Actions available to be executed

| User Role      | Action        | Description                                     | Pre Conditions                                                                 | Post Conditions                           |
| :------------- | :------------ | :---------------------------------------------- | :----------------------------------------------------------------------------- | :---------------------------------------- |
| Smart Contract | `setparams`   | Set parameter values                            | None                                                                           | Smart Contract allows to create proposals |
| Proposer       | `create`      | Create a new proposal                           | Account must hold `1000 LIBRE`                                                 | Proposal is created as draft              |
| Proposer       | `transfer`    | Pay for a new proposal                          | Account must pay cost of a new proposal                                        | Proposal is set as active for voting      |
| Voter          | `votefor`     | Vote for an active proposal                     | proposal must be active , voter must be registered and have a non-zero balance | Vote for count increases                  |
| Voter          | `voteagainst` | Vote against an active proposal                 | proposal must be active , voter must be registered and have a non-zero balance | Vote against count increases              |
| Approver       | `approve`     | Approve a Proposal                              | Proposal must have succeeded                                                   | Proposal is executed                      |
| Approver       | `reject`      | Reject a Proposal                               | Proposal must have succeeded                                                   | Proposal is executed                      |
| Open           | `countvotes`  | Count votes to determine if a proposal succeeds | Proposal must have completed voting window                                     | Proposal is approved or cancelled         |
| Open           | `checkvotes`  | Count votes of a proposal                       | Proposal must be active                                                        | Proposal votes (for/against) are updated  |

## Cleos commands

### Set Params

```bash
cleos push action dao.libre setparams '{"funding_account": "dao.libre", "vote_threshold": 3, "voting_days": 1, "minimum_vp_to_create_proposals": 10, "proposal_cost": "5.0000 LIBRE", "approver": "dao.libre"}' -p dao.libre@active
```

### Create Proposal

```bash
cleos push action dao.libre create '{"creator": "alice", "receiver": "alice", "name": "testproposal", "title": "Test Proposal", "detail": "This is a test proposal", "amount": "100.0000 LIBRE", "url": "libre.org"}' -p alice@active
```

> This is required to activate the proposal for voting

```bash
cleos push action eosio.token transfer '{"from": "alice", "to": "dao.libre", "quantity": "5.0000 LIBRE", "memo": "payment:testproposal"}' -p alice@active
```

### Vote For/Against

```bash
cleos push action dao.libre votefor '{"voter": "bob", "proposal": "testproposal"}' -p bob@active

or

cleos push action dao.libre voteagainst '{"voter": "bob", "proposal": "testproposal"}' -p bob@active
```

### Check Votes

```bash
cleos push action dao.libre checkvotes '{"proposal": "testproposal", "max_steps": 100}' -p dao.libre@active
```

### Count Votes

```bash
cleos push action dao.libre countvotes '{"proposal": "testproposal", "max_steps": 100}' -p dao.libre@active
```

### Approve/Reject

```bash
cleos push action dao.libre approve '{"proposal": "testproposal"}' -p dao.libre@active

or

cleos push action dao.libre reject '{"proposal": "testproposal"}' -p dao.libre@active
```
