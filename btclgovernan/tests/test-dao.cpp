#include <tester-base.hpp>

#define CATCH_CONFIG_RUNNER

TEST_CASE( "invalid transfer" ) {
  tester t;

  t.fund_accounts();

  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "dao.libre"_n,
                                           s2a( "10.0000 LIBRE" ),
                                           "donation" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "dao.libre"_n,
                                           s2a( "10.0000 LIBRE" ),
                                           "founding" );

  expect( t.alice.trace< token::actions::transfer >( "alice"_n,
                                                     "dao.libre"_n,
                                                     s2a( "10.0000 LIBRE" ),
                                                     "1donation" ),
          "invalid memo" );
  expect( t.alice.trace< token::actions::transfer >( "alice"_n,
                                                     "dao.libre"_n,
                                                     s2a( "10.0000 LIBRE" ),
                                                     "1founding" ),
          "invalid memo" );
}

TEST_CASE( "set params" ) {
  tester t;

  t.fund_accounts();

  expect( t.alice.trace< dao::actions::setparams >( "dao.libre"_n,
                                                    1,
                                                    10,
                                                    10000,
                                                    s2a( "2000.0000 LIBRE" ),
                                                    "dao.libre"_n ),
          "Missing required authority" );
  expect( t.daolibre.trace< dao::actions::setparams >( "dao.libre"_n,
                                                       1,
                                                       10,
                                                       -1,
                                                       s2a( "2000.0000 LIBRE" ),
                                                       "dao.libre"_n ),
          "minimum_vp_to_create_proposals must be greater than 0" );
  expect( t.daolibre.trace< dao::actions::setparams >( "dao.libre"_n,
                                                       1,
                                                       10,
                                                       10000,
                                                       s2a( "2000 LIBRE" ),
                                                       "dao.libre"_n ),
          "invalid proposal_cost symbol" );
  expect( t.daolibre.trace< dao::actions::setparams >( "dao.libre"_n,
                                                       1,
                                                       10,
                                                       10000,
                                                       s2a( "2000.0000 OTHER" ),
                                                       "dao.libre"_n ),
          "invalid proposal_cost symbol" );

  t.daolibre.act< dao::actions::setparams >( "dao.libre"_n,
                                             1,
                                             10,
                                             10000,
                                             s2a( "2000.0000 LIBRE" ),
                                             "dao.libre"_n );

  dao::params2 expected{ .funding_account = "dao.libre"_n,
                         .vote_threshold = 1,
                         .voting_days = 10,
                         .minimum_vp_to_create_proposals = 10000,
                         .proposal_cost = s2a( "2000.0000 LIBRE" ),
                         .approver = "dao.libre"_n };
  dao::params2 returned = t.get_params();

  CHECK( returned.funding_account == expected.funding_account );
  CHECK( returned.vote_threshold == expected.vote_threshold );
  CHECK( returned.voting_days == expected.voting_days );
  CHECK( returned.minimum_vp_to_create_proposals ==
         expected.minimum_vp_to_create_proposals );
  CHECK( returned.proposal_cost == expected.proposal_cost );
  CHECK( returned.approver == expected.approver );
}

TEST_CASE( "check proposal restrictions" ) {
  tester t;

  t.fund_accounts();

  expect(
      t.alice.trace< dao::actions::create >( "bob"_n,
                                             "alice"_n,
                                             "proposaltest"_n,
                                             std::string{ "proposal title" },
                                             std::string{ "proposal detail" },
                                             s2a( "2000.0000 LIBRE" ),
                                             std::string{ "url" } ),
      "Missing required authority" );
  expect(
      t.alice.trace< dao::actions::create >( "alice"_n,
                                             "alice"_n,
                                             "proposaltest"_n,
                                             std::string{ "proposal title" },
                                             std::string{ "proposal detail" },
                                             s2a( "2000.0000 OTHER" ),
                                             std::string{ "url" } ),
      "invalid token symbol" );
  expect(
      t.alice.trace< dao::actions::create >( "alice"_n,
                                             "alice"_n,
                                             "proposaltest"_n,
                                             std::string{ "proposal title" },
                                             std::string{ "proposal detail" },
                                             s2a( "0.0000 LIBRE" ),
                                             std::string{ "url" } ),
      "minimum amount is: 0.0001 LIBRE" );
  expect(
      t.alice.trace< dao::actions::create >( "alice"_n,
                                             "fakeaccount"_n,
                                             "proposaltest"_n,
                                             std::string{ "proposal title" },
                                             std::string{ "proposal detail" },
                                             s2a( "2000.0000 LIBRE" ),
                                             std::string{ "url" } ),
      "receiver account does not exist" );
  expect(
      t.alice.trace< dao::actions::create >(
          "alice"_n,
          "alice"_n,
          "proposaltest"_n,
          std::string{ "proposal title proposal title proposal title proposal "
                       "title proposal title proposal title proposal ti" },
          std::string{ "proposal detail" },
          s2a( "2000.0000 LIBRE" ),
          std::string{ "url" } ),
      "max title size is 100 characters" );
  expect(
      t.alice.trace< dao::actions::create >(
          "alice"_n,
          "alice"_n,
          "proposaltest"_n,
          std::string{ "proposal title" },
          std::string{
              "proposal detail proposal detail proposal detail proposal detail "
              "proposal detail proposal detail proposal detail proposal detail "
              "proposal detail proposal detail proposal detail proposal detail "
              "proposal detail proposal detail proposal detail proposal detail "
              "proposal detail proposal detail proposal detail proposal detail "
              "proposal detail proposal detail proposal detail proposal detail "
              "proposal detail proposal detail proposal detail proposal detail "
              "proposal detail proposal detail proposal detail proposal detail "
              "proposal detail proposal detail proposal detail proposal detail "
              "proposal detail proposal detail proposal detail proposal detail "
              "proposal detail proposal detail proposal detail proposal detail "
              "proposal detail proposal detail proposal detail proposal detail "
              "proposal detail proposal detail proposal detail proposal detail "
              "proposal detail proposal detail proposal detail proposal detail "
              "proposal detail proposal detail proposal detail proposal detail "
              "proposal detail proposal detail proposal " },
          s2a( "2000.0000 LIBRE" ),
          std::string{ "url" } ),
      "max detail size is 1000 characters" );
  expect(
      t.alice.trace< dao::actions::create >(
          "alice"_n,
          "alice"_n,
          "proposaltest"_n,
          std::string{ "proposal title" },
          std::string{ "proposal detail" },
          s2a( "2000.0000 LIBRE" ),
          std::string{ "url url url url url url url url url url url url url "
                       "url url url url url url url url url url url url u" } ),
      "max url size is 100 characters" );
  expect(
      t.alice.trace< dao::actions::create >( "alice"_n,
                                             "alice"_n,
                                             "proposaltest"_n,
                                             std::string{ "proposal title" },
                                             std::string{ "proposal detail" },
                                             s2a( "2000.0000 LIBRE" ),
                                             std::string{ "url" } ),
      "missing params" );

  t.daolibre.act< dao::actions::setparams >( "dao.libre"_n,
                                             1,
                                             10,
                                             10000,
                                             s2a( "2000.0000 LIBRE" ),
                                             "dao.libre"_n );

  expect(
      t.alice.trace< dao::actions::create >( "alice"_n,
                                             "alice"_n,
                                             "proposaltest"_n,
                                             std::string{ "proposal title" },
                                             std::string{ "proposal detail" },
                                             s2a( "2000.0000 LIBRE" ),
                                             std::string{ "url" } ),
      "not enough voting power" );

  t.start_staking( "2022-07-04T00:00:00.000" );
  t.fully_stake();

  t.alice.act< dao::actions::create >( "alice"_n,
                                       "alice"_n,
                                       "proposaltest"_n,
                                       std::string{ "proposal title" },
                                       std::string{ "proposal detail" },
                                       s2a( "0.0001 LIBRE" ),
                                       std::string{ "url" } );
  t.chain.start_block();

  expect(
      t.alice.trace< dao::actions::create >( "alice"_n,
                                             "alice"_n,
                                             "proposaltest"_n,
                                             std::string{ "proposal title" },
                                             std::string{ "proposal detail" },
                                             s2a( "2000.0000 LIBRE" ),
                                             std::string{ "url" } ),
      "name already in use" );

  std::map< eosio::name, uint8_t > expected = {
      { "proposaltest"_n, dao::proposal_status::DRAFT } };

  CHECK( t.get_proposals() == expected );
}

TEST_CASE( "create and fund a proposal" ) {
  tester t;

  t.fund_accounts();
  t.start_staking( "2022-07-04T00:00:00.000" );
  t.fully_stake();

  t.daolibre.act< dao::actions::setparams >( "dao.libre"_n,
                                             1,
                                             10,
                                             10000,
                                             s2a( "2000.0000 LIBRE" ),
                                             "dao.libre"_n );
  t.alice.act< dao::actions::create >( "alice"_n,
                                       "alice"_n,
                                       "proposaltest"_n,
                                       std::string{ "proposal title" },
                                       std::string{ "proposal detail" },
                                       s2a( "5000.0000 LIBRE" ),
                                       std::string{ "url" } );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "dao.libre"_n,
                                           s2a( "2000.0000 LIBRE" ),
                                           "payment:proposaltest" );

  std::map< eosio::name, uint8_t > expected = {
      { "proposaltest"_n, dao::proposal_status::ACTIVE } };

  CHECK( t.get_proposals() == expected );
}

TEST_CASE( "vote for a proposal" ) {
  tester t;

  t.fund_accounts();
  t.start_staking( "2022-07-04T00:00:00.000" );
  t.fully_stake();

  t.daolibre.act< dao::actions::setparams >( "dao.libre"_n,
                                             1,
                                             10,
                                             10000,
                                             s2a( "2000.0000 LIBRE" ),
                                             "dao.libre"_n );
  t.alice.act< dao::actions::create >( "alice"_n,
                                       "alice"_n,
                                       "proposaltest"_n,
                                       std::string{ "proposal title" },
                                       std::string{ "proposal detail" },
                                       s2a( "5000.0000 LIBRE" ),
                                       std::string{ "url" } );

  expect( t.alice.trace< dao::actions::votefor >( "alice"_n, "proposaltest"_n ),
          "invalid proposal status" );

  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "dao.libre"_n,
                                           s2a( "2000.0000 LIBRE" ),
                                           "payment:proposaltest" );

  expect( t.bob.trace< dao::actions::votefor >( "bob"_n, "proposaltest"_n ),
          "not enough voting power" );
  expect( t.bob.trace< dao::actions::votefor >( "alice"_n, "proposaltest"_n ),
          "Missing required authority" );
  expect( t.alice.trace< dao::actions::votefor >( "alice"_n, "fakeproposal"_n ),
          "proposal not found" );

  t.skip_to( "2022-07-14T00:00:00.000" );

  t.alice.act< dao::actions::votefor >( "alice"_n, "proposaltest"_n );

  t.chain.start_block();
  t.chain.start_block();

  expect( t.pip.trace< dao::actions::votefor >( "pip"_n, "proposaltest"_n ),
          "the voting period is over" );

  t.alice.act< dao::actions::create >( "alice"_n,
                                       "alice"_n,
                                       "proposal1"_n,
                                       std::string{ "proposal title" },
                                       std::string{ "proposal detail" },
                                       s2a( "5000.0000 LIBRE" ),
                                       std::string{ "url" } );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "dao.libre"_n,
                                           s2a( "2000.0000 LIBRE" ),
                                           "payment:proposal1" );
  t.alice.act< dao::actions::votefor >( "alice"_n, "proposal1"_n );

  t.chain.start_block();

  expect( t.alice.trace< dao::actions::votefor >( "alice"_n, "proposal1"_n ),
          "already voted" );

  t.pip.act< dao::actions::voteagainst >( "pip"_n, "proposal1"_n );

  std::map< eosio::name, bool > expected = { { "alice"_n, true },
                                             { "pip"_n, false } };

  CHECK( t.get_votes( "proposal1"_n ) == expected );
}

TEST_CASE( "check voting power is updated on voter for and voteagainst" ) {
  tester t;

  t.fund_accounts();
  t.start_staking( "2022-07-04T00:00:00.000" );
  t.fully_stake();

  t.daolibre.act< dao::actions::setparams >( "dao.libre"_n,
                                             1,
                                             10,
                                             10000,
                                             s2a( "2000.0000 LIBRE" ),
                                             "dao.libre"_n );
  t.alice.act< dao::actions::create >( "alice"_n,
                                       "alice"_n,
                                       "proposaltest"_n,
                                       std::string{ "proposal title" },
                                       std::string{ "proposal detail" },
                                       s2a( "5000.0000 LIBRE" ),
                                       std::string{ "url" } );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "dao.libre"_n,
                                           s2a( "2000.0000 LIBRE" ),
                                           "payment:proposaltest" );

  auto returned_proposal = t.get_proposal( "proposaltest"_n );

  CHECK( returned_proposal.votes_for == 0 );
  CHECK( returned_proposal.votes_against == 0 );

  t.alice.act< dao::actions::votefor >( "alice"_n, "proposaltest"_n );
  t.pip.act< dao::actions::voteagainst >( "pip"_n, "proposaltest"_n );

  returned_proposal = t.get_proposal( "proposaltest"_n );

  CHECK( returned_proposal.votes_for == 4000000 );
  CHECK( returned_proposal.votes_against == 4000000 );
}

TEST_CASE( "checkvotes for a proposal" ) {
  tester t;

  t.fund_accounts();
  t.start_staking( "2022-07-04T00:00:00.000" );
  t.fully_stake();

  expect( t.alice.trace< dao::actions::checkvotes >( "proposaltest"_n, 100 ),
          "proposal not found" );

  t.daolibre.act< dao::actions::setparams >( "dao.libre"_n,
                                             1,
                                             10,
                                             10000,
                                             s2a( "2000.0000 LIBRE" ),
                                             "dao.libre"_n );
  t.alice.act< dao::actions::create >( "alice"_n,
                                       "alice"_n,
                                       "proposaltest"_n,
                                       std::string{ "proposal title" },
                                       std::string{ "proposal detail" },
                                       s2a( "5000.0000 LIBRE" ),
                                       std::string{ "url" } );
  t.alice.act< dao::actions::create >( "alice"_n,
                                       "alice"_n,
                                       "proposal1"_n,
                                       std::string{ "proposal title" },
                                       std::string{ "proposal detail" },
                                       s2a( "5000.0000 LIBRE" ),
                                       std::string{ "url" } );

  expect( t.alice.trace< dao::actions::checkvotes >( "proposaltest"_n, 100 ),
          "invalid proposal status" );

  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "dao.libre"_n,
                                           s2a( "2000.0000 LIBRE" ),
                                           "payment:proposaltest" );

  t.alice.act< dao::actions::votefor >( "alice"_n, "proposaltest"_n );
  t.pip.act< dao::actions::votefor >( "pip"_n, "proposaltest"_n );
  t.egeon.act< dao::actions::voteagainst >( "egeon"_n, "proposaltest"_n );
  t.alice.act< dao::actions::checkvotes >( "proposaltest"_n, 2 );
  t.alice.act< dao::actions::checkvotes >( "proposaltest"_n, 1 );

  expect( t.alice.trace< dao::actions::checkvotes >( "proposaltest"_n, 100 ),
          "the action was already executed in the last 5 minutes" );

  dao::proposals expected = { .name = "proposaltest"_n,
                              .votes_for = 4000000 * 2,
                              .votes_against = 4000000,
                              .status = dao::proposal_status::ACTIVE };
  auto           result = t.get_proposal( "proposaltest"_n );

  CHECK( result.name == expected.name );
  CHECK( result.votes_for == expected.votes_for );
  CHECK( result.votes_against == expected.votes_against );
  CHECK( result.status == expected.status );
}

TEST_CASE( "countvotes for a proposal" ) {
  tester t;

  t.fund_accounts();
  t.start_staking( "2022-07-04T00:00:00.000" );
  t.fully_stake();

  expect( t.alice.trace< dao::actions::countvotes >( "proposaltest"_n, 100 ),
          "proposal not found" );

  t.daolibre.act< dao::actions::setparams >( "dao.libre"_n,
                                             1,
                                             10,
                                             10000,
                                             s2a( "2000.0000 LIBRE" ),
                                             "dao.libre"_n );
  t.alice.act< dao::actions::create >( "alice"_n,
                                       "alice"_n,
                                       "proposaltest"_n,
                                       std::string{ "proposal title" },
                                       std::string{ "proposal detail" },
                                       s2a( "5000.0000 LIBRE" ),
                                       std::string{ "url" } );

  expect( t.alice.trace< dao::actions::countvotes >( "proposaltest"_n, 100 ),
          "invalid proposal status" );

  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "dao.libre"_n,
                                           s2a( "2000.0000 LIBRE" ),
                                           "payment:proposaltest" );

  t.alice.act< dao::actions::votefor >( "alice"_n, "proposaltest"_n );
  t.pip.act< dao::actions::votefor >( "pip"_n, "proposaltest"_n );
  t.egeon.act< dao::actions::votefor >( "egeon"_n, "proposaltest"_n );
  t.bertie.act< dao::actions::voteagainst >( "bertie"_n, "proposaltest"_n );
  t.ahab.act< dao::actions::voteagainst >( "ahab"_n, "proposaltest"_n );

  t.skip_to( "2022-07-14T00:00:00.000" );

  expect( t.alice.trace< dao::actions::countvotes >( "proposaltest"_n, 100 ),
          "the voting period is not yet over" );

  t.chain.start_block();
  t.chain.start_block();

  t.alice.act< dao::actions::countvotes >( "proposaltest"_n, 1 );
  t.alice.act< dao::actions::countvotes >( "proposaltest"_n, 2 );
  t.alice.act< dao::actions::countvotes >( "proposaltest"_n, 3 );

  dao::proposals expected = { .name = "proposaltest"_n,
                              .votes_for = 4000000 * 3,
                              .votes_against = 4000000 * 2,
                              .status = dao::proposal_status::SUCCEEDED };
  auto           result = t.get_proposal( "proposaltest"_n );

  CHECK( result.name == expected.name );
  CHECK( result.votes_for == expected.votes_for );
  CHECK( result.votes_against == expected.votes_against );
  CHECK( result.status == expected.status );
}

TEST_CASE( "approve a proposal" ) {
  tester t;

  t.fund_accounts();
  t.start_staking( "2022-07-04T00:00:00.000" );
  t.fully_stake();

  t.daolibre.act< dao::actions::setparams >( "dao.libre"_n,
                                             1,
                                             10,
                                             10000,
                                             s2a( "2000.0000 LIBRE" ),
                                             "dao.libre"_n );
  t.alice.act< dao::actions::create >( "alice"_n,
                                       "alice"_n,
                                       "proposaltest"_n,
                                       std::string{ "proposal title" },
                                       std::string{ "proposal detail" },
                                       s2a( "5000.0000 LIBRE" ),
                                       std::string{ "url" } );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "dao.libre"_n,
                                           s2a( "2000.0000 LIBRE" ),
                                           "payment:proposaltest" );
  t.alice.act< dao::actions::votefor >( "alice"_n, "proposaltest"_n );
  t.pip.act< dao::actions::votefor >( "pip"_n, "proposaltest"_n );
  t.egeon.act< dao::actions::voteagainst >( "egeon"_n, "proposaltest"_n );

  t.skip_to( "2022-07-14T00:00:00.000" );

  t.chain.start_block();
  t.chain.start_block();

  expect( t.alice.trace< dao::actions::approve >( "fakeproposal"_n ),
          "proposal not found" );
  expect( t.alice.trace< dao::actions::approve >( "proposaltest"_n ),
          "invalid proposal status" );

  t.alice.act< dao::actions::countvotes >( "proposaltest"_n, 100 );

  expect( t.alice.trace< dao::actions::approve >( "proposaltest"_n ),
          "Missing required authority" );

  t.daolibre.act< dao::actions::approve >( "proposaltest"_n );

  CHECK( t.get_proposal( "proposaltest"_n ).status ==
         dao::proposal_status::COMPLETED );
}

TEST_CASE( "reject a proposal" ) {
  tester t;

  t.fund_accounts();
  t.start_staking( "2022-07-04T00:00:00.000" );
  t.fully_stake();

  t.daolibre.act< dao::actions::setparams >( "dao.libre"_n,
                                             1,
                                             10,
                                             10000,
                                             s2a( "2000.0000 LIBRE" ),
                                             "dao.libre"_n );
  t.alice.act< dao::actions::create >( "alice"_n,
                                       "alice"_n,
                                       "proposaltest"_n,
                                       std::string{ "proposal title" },
                                       std::string{ "proposal detail" },
                                       s2a( "5000.0000 LIBRE" ),
                                       std::string{ "url" } );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "dao.libre"_n,
                                           s2a( "2000.0000 LIBRE" ),
                                           "payment:proposaltest" );
  t.alice.act< dao::actions::votefor >( "alice"_n, "proposaltest"_n );
  t.pip.act< dao::actions::votefor >( "pip"_n, "proposaltest"_n );
  t.egeon.act< dao::actions::voteagainst >( "egeon"_n, "proposaltest"_n );

  t.skip_to( "2022-07-14T00:00:00.000" );

  t.chain.start_block();
  t.chain.start_block();

  expect( t.alice.trace< dao::actions::reject >( "fakeproposal"_n ),
          "proposal not found" );
  expect( t.alice.trace< dao::actions::reject >( "proposaltest"_n ),
          "invalid proposal status" );

  t.alice.act< dao::actions::countvotes >( "proposaltest"_n, 100 );

  expect( t.alice.trace< dao::actions::reject >( "proposaltest"_n ),
          "Missing required authority" );

  t.daolibre.act< dao::actions::reject >( "proposaltest"_n );

  CHECK( t.get_proposal( "proposaltest"_n ).status ==
         dao::proposal_status::CANCELED );
}