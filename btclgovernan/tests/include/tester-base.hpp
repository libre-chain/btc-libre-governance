#include <eosio/tester.hpp>

#include <btclgovernan.hpp>
#include <stakingtoken.hpp>
#include <token/token.hpp>

// Catch2 unit testing framework. https://github.com/catchorg/Catch2
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

using namespace eosio;
using user_context = test_chain::user_context;

void daolibre_setup( test_chain &t ) {
  t.create_code_account( "dao.libre"_n );
  t.set_code( "dao.libre"_n, "btclgovernan.wasm" );
}

void staking_setup( test_chain &t ) {
  t.create_code_account( "stake.libre"_n );
  t.set_code( "stake.libre"_n, "stakingtoken.wasm" );
}

void token_setup( test_chain &t ) {
  t.create_code_account( "eosio.token"_n );
  t.set_code( "eosio.token"_n, "token.wasm" );

  t.as( "eosio.token"_n )
      .act< token::actions::create >( "eosio.token"_n,
                                      s2a( "10000000000.0000 LIBRE" ) );
  t.as( "eosio.token"_n )
      .act< token::actions::issue >( "eosio.token"_n,
                                     s2a( "366756575.6893 LIBRE" ),
                                     "" );

  t.as( "eosio.token"_n )
      .act< token::actions::create >( "eosio.token"_n,
                                      s2a( "10000000000.0000 OTHER" ) );
  t.as( "eosio.token"_n )
      .act< token::actions::issue >( "eosio.token"_n,
                                     s2a( "10000000000.0000 OTHER" ),
                                     "" );
}

struct tester {
  test_chain   chain;
  user_context daolibre = chain.as( "dao.libre"_n );
  user_context bitcoinlibre = chain.as( "bitcoinlibre"_n );
  user_context stakelibre = chain.as( "stake.libre"_n );

  user_context alice = chain.as( "alice"_n );
  user_context bob = chain.as( "bob"_n );
  user_context pip = chain.as( "pip"_n );
  user_context egeon = chain.as( "egeon"_n );
  user_context bertie = chain.as( "bertie"_n );
  user_context ahab = chain.as( "ahab"_n );

  tester() {
    daolibre_setup( chain );
    staking_setup( chain );
    token_setup( chain );

    for ( auto account : { "bitcoinlibre"_n,
                           "alice"_n,
                           "bob"_n,
                           "pip"_n,
                           "egeon"_n,
                           "bertie"_n,
                           "ahab"_n } ) {
      chain.create_account( account );
    }
  }

  void skip_to( std::string_view time ) {
    uint64_t value;
    check( string_to_utc_microseconds( value,
                                       time.data(),
                                       time.data() + time.size() ),
           "bad time" );

    time_point tp{ microseconds( value ) };

    chain.finish_block();
    auto head_tp = chain.get_head_block_info().timestamp.to_time_point();
    auto skip = ( tp - head_tp ).count() / 1000 - 500;
    chain.start_block( skip );
  }

  void fund_accounts() {
    for ( auto account :
          { "alice"_n, "bob"_n, "pip"_n, "egeon"_n, "bertie"_n, "ahab"_n } ) {
      chain.as( "eosio.token"_n )
          .act< token::actions::transfer >( "eosio.token"_n,
                                            account,
                                            s2a( "20000000.0000 LIBRE" ),
                                            "memo" );
      chain.as( "eosio.token"_n )
          .act< token::actions::transfer >( "eosio.token"_n,
                                            account,
                                            s2a( "20000000.0000 OTHER" ),
                                            "memo" );
    }

    chain.as( "eosio.token"_n )
        .act< token::actions::transfer >( "eosio.token"_n,
                                          "dao.libre",
                                          s2a( "20000000.0000 LIBRE" ),
                                          "donation" );
  }

  void start_staking( std::string_view date ) {
    skip_to( date );

    stakelibre.act< libre::actions::init >( time_point_sec{ 1656892800 }, 365 );
  }

  void fully_stake() {
    for ( auto account :
          { "alice"_n, "pip"_n, "egeon"_n, "bertie"_n, "ahab"_n } ) {
      chain.as( account ).act< token::actions::transfer >(
          account,
          "stake.libre"_n,
          s2a( "2000000.0000 LIBRE" ),
          "stakefor:365" );
    }
  }

  dao::params2 get_params() {
    dao::params2_singleton param_sing{ "dao.libre"_n, "dao.libre"_n.value };

    return param_sing.get();
  };

  dao::proposals get_proposal( eosio::name proposal ) const {
    dao::proposal_table proposal_tb{ "dao.libre"_n, "dao.libre"_n.value };

    auto itr = proposal_tb.find( proposal.value );

    return itr != proposal_tb.end() ? *itr : dao::proposals{};
  };

  auto get_proposals() const {
    std::map< eosio::name, uint8_t > result;
    dao::proposal_table proposal_tb{ "dao.libre"_n, "dao.libre"_n.value };

    for ( auto t : proposal_tb ) {
      result.insert( std::pair( t.name, t.status ) );
    }

    return result;
  };

  auto get_votes( eosio::name proposal ) const {
    std::map< eosio::name, bool > result;
    dao::vote_table               vote_tb{ "dao.libre"_n, proposal.value };

    for ( auto t : vote_tb ) {
      result.insert( std::pair( t.voter, t.is_for ) );
    }

    return result;
  };
};