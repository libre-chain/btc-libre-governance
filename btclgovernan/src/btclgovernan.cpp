/*
 *
 * @author  Edenia.com [https://edenia.com]
 *
 * @section DESCRIPTION
 *  Source file for the actions related with the btclgovernan contract
 *
 *  GitHub: https://gitlab.com/libre-chain/btc-libre-governance
 *
 */

#include <btclgovernan.hpp>
#include <stakingtoken.hpp>

namespace dao {
  void btclgovernan::setparams( eosio::name  funding_account,
                                uint8_t      vote_threshold,
                                uint8_t      voting_days,
                                int64_t      minimum_vp_to_create_proposals,
                                eosio::asset proposal_cost,
                                eosio::name  approver ) {
    require_auth( get_self() );

    eosio::check( minimum_vp_to_create_proposals >= 0,
                  "minimum_vp_to_create_proposals must be greater than 0" );
    eosio::check( proposal_cost.symbol ==
                      SUPPORTED_PAY_TOKEN_SYMBOL,
                  "invalid proposal_cost symbol" );

    auto data = param_sing.get_or_create( get_self() );
    data.funding_account = funding_account;
    data.vote_threshold = vote_threshold;
    data.voting_days = voting_days;
    data.minimum_vp_to_create_proposals = minimum_vp_to_create_proposals;
    data.proposal_cost = proposal_cost;
    data.approver = approver;
    param_sing.set( data, get_self() );
  }

  void btclgovernan::create( eosio::name  creator,
                             eosio::name  receiver,
                             eosio::name  name,
                             std::string  title,
                             std::string  detail,
                             eosio::asset amount,
                             std::string  url ) {
    require_auth( creator );

    eosio::check( amount.symbol == SUPPORTED_VOTE_TOKEN_SYMBOL,
                  "invalid token symbol" );
    eosio::check( amount >= MIN_CREATE_PROPOSAL_AMOUNT,
                  "minimum amount is: " +
                      MIN_CREATE_PROPOSAL_AMOUNT.to_string() );
    eosio::check( is_account( receiver ), "receiver account does not exist" );
    eosio::check( title.size() <= 100, "max title size is 100 characters" );
    eosio::check( detail.size() <= 1000, "max detail size is 1000 characters" );
    eosio::check( url.size() <= 100, "max url size is 100 characters" );

    auto proposal_itr = proposal_tb.find( name.value );

    eosio::check( proposal_itr == proposal_tb.end(), "name already in use" );
    eosio::check( param_sing.exists(), "missing params" );

    auto params_data = param_sing.get();

    eosio::check( get_voting_power( creator ) >=
                      params_data.minimum_vp_to_create_proposals,
                  "not enough voting power" );

    // save the proposal as a draft
    proposal_tb.emplace( get_self(), [&]( auto &row ) {
      row.creator = creator;
      row.receiver = receiver;
      row.name = name;
      row.title = title;
      row.detail = detail;
      row.amount = amount;
      row.url = url;
      row.created_on = eosio::current_time_point();
      row.votes_for = 0;
      row.votes_against = 0;
      row.status = proposal_status::DRAFT;
    } );
  }

  void btclgovernan::migrate() {
    params_singleton params_sing( get_self(), get_self().value );

    eosio::check( params_sing.exists(), "Table is already empty" );

    params_sing.remove();
  }

  void btclgovernan::notify_transfer( eosio::name  from,
                                      eosio::name  to,
                                      eosio::asset quantity,
                                      std::string  memo ) {
    // skip transactions that are not for btclgovernan
    if ( from == get_self() || to != get_self() ) {
      return;
    }

    eosio::check( EOSIO_TOKEN_CONTRACT == get_first_receiver(),
                  "invalid contract" );

    if ( memo.rfind( PAYMENT_TRANSFER, 0 ) == 0 ) {
      payment_handler( eosio::name( memo.substr( PAYMENT_TRANSFER.size() ) ),
                       quantity );
      return;
    }

    if ( from != STAKING_TOKEN_CONTRACT ) {
      eosio::check( memo.rfind( DONATION_TRANSFER, 0 ) == 0 ||
                        memo.rfind( FOUNDING_TRANSFER, 0 ) == 0,
                    "invalid memo" );
    }
  }

  void btclgovernan::votefor( eosio::name voter, eosio::name proposal ) {
    require_auth( voter );

    save_vote( voter, proposal, true );
  }

  void btclgovernan::voteagainst( eosio::name voter, eosio::name proposal ) {
    require_auth( voter );

    save_vote( voter, proposal, false );
  }

  void btclgovernan::countvotes( eosio::name proposal, uint32_t max_steps ) {
    auto proposal_itr = proposal_tb.find( proposal.value );
    eosio::check( proposal_itr != proposal_tb.end(), "proposal not found" );
    eosio::check( proposal_itr->status == proposal_status::ACTIVE,
                  "invalid proposal status" );
    // validate if the proposal is ready to count votes
    eosio::check( proposal_itr->expires_on.sec_since_epoch() <
                      eosio::current_time_point().sec_since_epoch(),
                  "the voting period is not yet over" );

    state_table state_tb( get_self(), proposal.value );
    auto        state_itr = state_tb.find( "countvotes"_n.value );

    // TODO: this logic can be moved to the create action once all old proposal have been accepted or rejected
    if ( state_itr == state_tb.end() ) {
      state_itr = state_tb.emplace( get_self(), [&]( auto &row ) {
        row.action = "countvotes"_n;
        row.next_account = eosio::name{};
      } );
    }

    vote_table vote_tb{ get_self(), proposal.value };
    bool       is_starting = state_itr->next_account == eosio::name{};
    auto       it = is_starting ? vote_tb.begin()
                                : vote_tb.find( state_itr->next_account.value );
    int64_t    votes_for = is_starting ? 0 : proposal_itr->votes_for;
    int64_t    votes_against = is_starting ? 0 : proposal_itr->votes_against;

    for ( ; max_steps > 0 && it != vote_tb.end(); --max_steps, it++ ) {
      int64_t power = get_voting_power( it->voter );

      if ( it->is_for ) {
        votes_for += power;
      } else {
        votes_against += power;
      }

      vote_tb.modify( it, get_self(), [&]( auto &row ) {
        row.quantity = power;
      } );
    }

    proposal_tb.modify( proposal_itr, get_self(), [&]( auto &row ) {
      row.votes_for = votes_for;
      row.votes_against = votes_against;
    } );

    if ( it != vote_tb.end() ) {
      state_tb.modify( state_itr, eosio::same_payer, [&]( auto &row ) {
        row.next_account = it->voter;
      } );
    } else {
      // get LIBRE current supply
      stats_table stats_tb( EOSIO_TOKEN_CONTRACT,
                            SUPPORTED_VOTE_TOKEN_SYMBOL.code().raw() );
      auto        stat_itr = stats_tb.begin();

      eosio::check( stat_itr != stats_tb.end(),
                    "unable to read current supply" );

      auto  params_data = param_sing.get();
      float achieved_vote_threshold =
          (float)( votes_for ) / (float)stat_itr->supply.amount * 100 * 10000;
      bool has_valid_vote_threshold =
          achieved_vote_threshold >= params_data.vote_threshold;
      bool has_majority = votes_for > votes_against;

      proposal_tb.modify( proposal_itr, get_self(), [&]( auto &row ) {
        row.status = has_majority && has_valid_vote_threshold
                         ? proposal_status::SUCCEEDED
                         : proposal_status::DEFEATED;
      } );

      // gc
      state_tb.erase( state_itr );
      gc( "checkvotes"_n, proposal );
    }
  }

  void btclgovernan::checkvotes( eosio::name proposal, uint32_t max_steps ) {
    auto proposal_itr = proposal_tb.find( proposal.value );

    eosio::check( proposal_itr != proposal_tb.end(), "proposal not found" );
    eosio::check( proposal_itr->status == proposal_status::ACTIVE,
                  "invalid proposal status" );

    state_table state_tb( get_self(), proposal.value );
    auto        state_itr = state_tb.find( "checkvotes"_n.value );

    // TODO: this logic can be moved to the create action once all old proposal have been accepted or rejected
    if ( state_itr == state_tb.end() ) {
      state_itr = state_tb.emplace( get_self(), [&]( auto &row ) {
        row.action = "checkvotes"_n;
        row.next_account = eosio::name{};
      } );
    } else {
      eosio::check( state_itr->last_update.sec_since_epoch() <=
                        ( eosio::current_time_point() -
                          eosio::minutes( MIN_TIME_VOTES_UPDATE ) )
                            .sec_since_epoch(),
                    "the action was already executed in the last " +
                        std::to_string( MIN_TIME_VOTES_UPDATE ) + " minutes" );
    }

    vote_table vote_tb{ get_self(), proposal.value };
    bool       is_starting = state_itr->next_account == eosio::name{};
    auto       it = is_starting ? vote_tb.begin()
                                : vote_tb.find( state_itr->next_account.value );
    int64_t    votes_for = is_starting ? 0 : proposal_itr->votes_for;
    int64_t    votes_against = is_starting ? 0 : proposal_itr->votes_against;

    for ( ; max_steps > 0 && it != vote_tb.end(); --max_steps, it++ ) {
      int64_t power = get_voting_power( it->voter );

      if ( it->is_for ) {
        votes_for += power;
      } else {
        votes_against += power;
      }

      vote_tb.modify( it, get_self(), [&]( auto &row ) {
        row.quantity = power;
      } );
    }

    proposal_tb.modify( proposal_itr, get_self(), [&]( auto &row ) {
      row.votes_for = votes_for;
      row.votes_against = votes_against;
    } );

    if ( it != vote_tb.end() ) {
      state_tb.modify( state_itr, eosio::same_payer, [&]( auto &row ) {
        row.next_account = it->voter;
      } );
    } else {
      state_tb.modify( state_itr, eosio::same_payer, [&]( auto &row ) {
        row.next_account = eosio::name{};
        row.last_update = eosio::current_time_point() +
                          eosio::minutes( MIN_TIME_VOTES_UPDATE );
      } );
    }
  }

  void btclgovernan::approve( eosio::name proposal ) {
    auto proposal_itr = proposal_tb.find( proposal.value );

    eosio::check( proposal_itr != proposal_tb.end(), "proposal not found" );
    eosio::check( proposal_itr->status == proposal_status::SUCCEEDED,
                  "invalid proposal status" );

    auto params_data = param_sing.get();

    require_auth( params_data.approver );

    eosio::action(
        eosio::permission_level{ get_self(), eosio::name( "active" ) },
        EOSIO_TOKEN_CONTRACT,
        eosio::name( "transfer" ),
        std::make_tuple( params_data.funding_account,
                         proposal_itr->receiver,
                         proposal_itr->amount,
                         "funding for " + proposal_itr->title ) )
        .send();

    proposal_tb.modify( proposal_itr, get_self(), [&]( auto &row ) {
      row.status = proposal_status::COMPLETED;
    } );
  }

  void btclgovernan::reject( eosio::name proposal ) {
    auto proposal_itr = proposal_tb.find( proposal.value );

    eosio::check( proposal_itr != proposal_tb.end(), "proposal not found" );
    eosio::check( proposal_itr->status == proposal_status::SUCCEEDED,
                  "invalid proposal status" );

    auto params_data = param_sing.get();

    require_auth( params_data.approver );

    proposal_tb.modify( proposal_itr, get_self(), [&]( auto &row ) {
      row.status = proposal_status::CANCELED;
    } );
  }

  // helper functions
  int64_t btclgovernan::get_voting_power( eosio::name account ) {
    libre::power_table power_tb( STAKING_TOKEN_CONTRACT,
                                 STAKING_TOKEN_CONTRACT.value );
    auto               itr = power_tb.find( account.value );

    if ( itr == power_tb.end() ) {
      return 0;
    }

    // voting power is truncated on purpose
    return itr->voting_power;
  }

  void btclgovernan::payment_handler( eosio::name  proposal,
                                      eosio::asset quantity ) {
    auto proposal_itr = proposal_tb.find( proposal.value );

    eosio::check( proposal_itr != proposal_tb.end(), "proposal not found" );
    eosio::check( proposal_itr->status == proposal_status::DRAFT,
                  "invalid proposal status" );

    auto params_data = param_sing.get();

    eosio::check( quantity.symbol == SUPPORTED_PAY_TOKEN_SYMBOL,
                  "invalid quantity symbol" );
    eosio::check( quantity.amount == params_data.proposal_cost.amount,
                  "invalid quantity amount" );

    // change the proposal status to start the voting process
    proposal_tb.modify( proposal_itr, get_self(), [&]( auto &row ) {
      row.expires_on =
          eosio::current_time_point() + eosio::days( params_data.voting_days );
      row.status = proposal_status::ACTIVE;
    } );
  }

  void btclgovernan::save_vote( eosio::name voter,
                                eosio::name proposal,
                                bool        is_for ) {
    int64_t power = get_voting_power( voter );

    eosio::check( power > 0, "not enough voting power" );

    auto proposal_itr = proposal_tb.find( proposal.value );
    eosio::check( proposal_itr != proposal_tb.end(), "proposal not found" );
    eosio::check( proposal_itr->status == proposal_status::ACTIVE,
                  "invalid proposal status" );

    // validate if the proposal is still available to receive votes
    eosio::check( proposal_itr->expires_on.sec_since_epoch() >=
                      eosio::current_time_point().sec_since_epoch(),
                  "the voting period is over" );

    vote_table vote_tb{ get_self(), proposal.value };
    auto       vote_itr = vote_tb.find( voter.value );

    eosio::check( vote_itr == vote_tb.end(), "already voted" );

    vote_tb.emplace( get_self(), [&]( auto &row ) {
      row.voter = voter;
      row.is_for = is_for;
      row.quantity = power;
    } );

    proposal_tb.modify( proposal_itr, get_self(), [&]( auto &row ) {
      is_for ? row.votes_for += power : row.votes_against += power;
    } );
  }

  void btclgovernan::gc( eosio::name action, eosio::name proposal ) {
    state_table state_tb( get_self(), proposal.value );
    auto        state_itr = state_tb.find( action.value );

    if ( state_itr != state_tb.end() ) {
      state_tb.erase( state_itr );
    }
  }
} // namespace dao

EOSIO_ACTION_DISPATCHER( dao::actions )

EOSIO_ABIGEN( actions( dao::actions ),
              table( "params2"_n, dao::params2 ),
              table( "proposals"_n, dao::proposals ),
              table( "votes"_n, dao::votes ),
              table( "state"_n, dao::state ) )